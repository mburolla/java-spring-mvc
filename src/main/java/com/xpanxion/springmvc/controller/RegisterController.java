package com.xpanxion.springmvc.controller;

import com.xpanxion.springmvc.model.User;
import com.xpanxion.springmvc.service.GymService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class RegisterController {

    //
    // Data members
    //

    @Autowired
    private GymService gymService;

    //
    // Constructors
    //

    public RegisterController() {
    }

    //
    // Methods
    //

    @GetMapping("/register")
    public String showForm(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        model.addAttribute("activePage", "register"); // Highlights the item in the nav bar.
        return "register";
    }

    @PostMapping("/register")
    public String submitForm(@ModelAttribute("user") User user) {
        // TODO: Add to db...
        return "register_success";
    }
}
