# Java Spring MVC
A simple Spring MVC app that uses ThymeLeaf.

# Links
[YouTube NavBar](https://youtu.be/tzsNTcMHxis)

[Forms](https://www.baeldung.com/spring-mvc-form-tutorial)

# Notes
Built with IntelliJ IDEA 2021.2.2 (Community Edition)
